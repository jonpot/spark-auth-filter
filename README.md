# Spark Authentication Filter #

This provides a Basic Authentication Filter that can be used to provide authentication for use with Apache Spark Web UI.

### Building and Using ###

```bash
# Build the jar on gl-build.arc-ts.umich.edu.
mvn clean package

# Copy the jar to Spark jars directory ($SPARK_HOME/jars) for each spark installation for each cluster.
# Use your own software installer account.
sftp jonpotsw@gl-build.arc-ts.umich.edu
rm /sw/dis/centos7/spark/spark-3.5.1-bin-hadoop3/jars/basicAuthenticationFilter-1.0.*.jar
put target/*.jar /sw/dis/centos7/spark/spark-3.5.1-bin-hadoop3/jars/
chmod 644 /sw/pkgs/arc/spark/spark-3.5.1-bin-hadoop3/jars/basicAuthenticationFilter*
ls -l /sw/pkgs/arc/spark/spark-3.5.1-bin-hadoop3/jars/basicAuthenticationFilter*
quit

sftp jonpotsw@armis2-build.arc-ts.umich.edu
rm /sw/dis/centos7/spark/spark-3.5.1-bin-hadoop3/jars/basicAuthenticationFilter-1.0.*.jar
put target/*.jar  /sw/dis/centos7/spark/spark-3.5.1-bin-hadoop3/jars/
chmod 644 /sw/pkgs/arc/spark/spark-3.5.1-bin-hadoop3/jars/basicAuthenticationFilter*
ls -l /sw/pkgs/arc/spark/spark-3.5.1-bin-hadoop3/jars/basicAuthenticationFilter*
quit

# Add these lines to spark-defaults.conf with a strong password.
spark.ui.filters edu.umich.arc.jonpot.BasicAuthenticationFilter
spark.edu.umich.arc.jonpot.BasicAuthenticationFilter.params username=admin,password=<STRONG_PASSWORD>
```
